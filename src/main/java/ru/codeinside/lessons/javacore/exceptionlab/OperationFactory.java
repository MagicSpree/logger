package ru.codeinside.lessons.javacore.exceptionlab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class OperationFactory {

    private final Logger log = LogManager.getLogger(OperationFactory.class);

    public static void main(String[] args) {
        var operationFactory = new OperationFactory();
        operationFactory.parseAndDivide();
        operationFactory.checkLength();
    }

    private void parseAndDivide() {
        log.info("метод parseAndDivide() успешно стартовал.");
        List<String> source = List.of("2", "5", "0", "10", "10000000000", "-100", "qwerty");
        for (String element : source) {
            double result = getResultParseAndDivide(element);
            System.out.println(result);
        }
        log.info("метод parseAndDivide() завершился");
    }

    private void checkLength() {
        log.info("метод checkLength() успешно стартовал.");
        List<String> source = Arrays.asList("car", "table", "", "01", "alphabet", null, "zero");
        for (String element : source) {
            int length = getLengthString(element);
            System.out.println(length);
        }
        log.info("метод checkLength() завершился");
    }

    private double getResultParseAndDivide(String elementArray) {
        try {
            int value = Integer.parseInt(elementArray);
            double result = 1000 / value;
            return result;
        } catch (ArithmeticException errorException) {
            log.error("Ошибка арифметической операций: " + errorException.getMessage());
            return -1;
        }
        catch (NumberFormatException numberFormatException) {
            log.error("Ошибка перевода строки в число: " + numberFormatException.getMessage());
            return -1;
        }
    }

    private int getLengthString(String elementArray) {
        try {
            return elementArray.length();
        } catch (NullPointerException nullPointerException) {
            log.error("Обнаружен null");
            return -1;
        }
    }
}
